// Set the date we're counting down to
var now = new Date();
var countdownLabel = "The next jam starts in:";
var countdownDate = getJamStart(now.getFullYear(), now.getMonth());
// Is the jam already in progress?
if (countdownDate.getDate() < now.getDate()) {
  countdownLabel = "Submissions close in:";
  countdownDate.setDate(countdownDate.getDate() + 9);
}
// Are we voting?
if (countdownDate.getDate() < now.getDate()) {
  countdownLabel = "Voting ends in:";
  countdownDate.setDate(countdownDate.getDate() + 4);
}
// Have we finished for the month?
if (countdownDate.getDate() < now.getDate()) {
  countdownLabel = "The next jam starts in:";
  countdownDate = getJamStart(now.getFullYear(), now.getMonth() + 1);
}

function getJamStart(year, month) {
  var result = new Date(year, month);
  var saturdays = 0;
  while (saturdays < 2) {
    if (result.getDay() == 6) {
      saturdays++;
    }
    // Progress if we haven't already found the start of the jam
    if (saturdays != 2) {
      result.setDate(result.getDate() + 1);
    }
  }
  result.setUTCHours(23);
  return result;
}

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countdownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the right element
  var countdownString = days + " days, " + hours + " hours, " + minutes + " minutes, and " + seconds + " seconds.";
  document.getElementById("jam-countdown").innerHTML = countdownLabel + "<br>" + countdownString;

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("jam-countdown").innerHTML = "...";
  }
}, 1000);
